# -*- coding: utf-8 -*-
from datetime import date
import random
import src.apps.ITV.element_objects.payment_success_page_elements as payment_success_page
import  src.apps.ITV.element_objects.home_page_elements as home_page

email = 'test_jana_itv{}@mailinator.net'.format(random.randint(0, 10000000000))
password = 'aaaaaaaa'
card_name = 'Jana Test'
card_number = '4111111111111111'
expiry_month = '09'
expiry_year = '19'
cvv = '123'


def test_subscribe_to_one_off_plan(web_driver, register_new_user, submit_card_details, url, choose_plan):
    """
    Test: User can successfully subscribe to one off plan.
    """
    register_new_user(email=email, password=password)
    choose_plan(plan='holiday')
    submit_card_details(card_name=card_name, card_number=card_number, expiry_month=expiry_month,
                        expiry_year=expiry_year, cvv=cvv)
    success_string = "You are subscribed for 30 days\n€ 4.99 A one off payment for 30 days."
    assert payment_success_page.holiday_pass_success_page(web_driver).text == success_string.decode('utf-8')
    payment_success_page.click_ok_button(web_driver)
    home_page.wait_for_itv_logo(web_driver)
    assert web_driver.current_url == url


def test_subscribe_to_ongoing_plan(web_driver, register_new_user, submit_card_details, url, choose_plan):
    """
    Test: User can successfully subscribe to ongoing plan.
    """
    register_new_user(email=email, password=password)
    choose_plan(plan='ongoing')
    submit_card_details(card_name=card_name, card_number=card_number, expiry_month=expiry_month,
                        expiry_year=expiry_year, cvv=cvv)
    day = date.today().day
    success_string = "You are signed up to the monthly subscription\n€ 4.99 monthly on-going subscription.\nYour " \
                     "subscription will automatically renew at the end of the month.\nYou will be charged monthly on " \
                     "the {}th of the month\nYou can cancel your subscription from your account settings.".format(day)
    assert payment_success_page.ongoing_success_page(web_driver).text == success_string.decode('utf-8')
    payment_success_page.click_ok_button(web_driver)
    home_page.wait_for_itv_logo(web_driver)
    assert web_driver.current_url == url