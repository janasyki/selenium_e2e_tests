from src.apps.utils import wait_for_element_visibility


def enter_card_name(web_driver, name_on_card):
    element = wait_for_element_visibility(web_driver, "name", "nameOnCard")
    element.send_keys(name_on_card)


def enter_card_number(web_driver, card_number):
    element = wait_for_element_visibility(web_driver, "name", "cardNumber")
    element.send_keys(card_number)


def enter_expiry_date_month(web_driver, month):
    element = wait_for_element_visibility(web_driver, "css_selector",
                                          "input[ng-model=\"purchaseCtrl.details.expirationMonth\"]")
    element.send_keys(month)


def enter_expiry_date_year(web_driver, year):
    element = wait_for_element_visibility(web_driver, "css_selector",
                                          "input[ng-model=\"purchaseCtrl.details.expirationYear\"]")
    element.send_keys(year)


def enter_cvv_code(web_driver, cvv):
    element = wait_for_element_visibility(web_driver, "name", "cvv")
    element.send_keys(cvv)
