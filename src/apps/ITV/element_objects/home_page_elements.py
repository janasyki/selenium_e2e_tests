from src.apps.utils import wait_for_element_visibility


def click_sign_in_button(web_driver):
    wait_for_element_visibility(web_driver, "css_selector", "a[data-role='login-modal']").click()


def click_account_details(web_driver):
    wait_for_element_visibility(web_driver, "css_selector",
                                "ul[role='menu']>li:nth-child(1)").click()


def click_itv_logo(web_driver):
    wait_for_element_visibility(web_driver, "id", "svg-itv-logo").click()


def wait_for_itv_logo(web_driver):
    wait_for_element_visibility(web_driver, "id", "svg-itv-logo")