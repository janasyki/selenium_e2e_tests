from src.apps.utils import wait_for_element_visibility


def holiday_pass_success_page(web_driver):
    element = wait_for_element_visibility(web_driver,
                                          "css_selector",
                                          "div[ng-if=\"subscribeSuccessCtrl.selectedPlan.type == 'holiday'\"]",
                                          timeout=6)
    return element


def ongoing_success_page(web_driver):
    element = wait_for_element_visibility(web_driver,
                                          "css_selector",
                                          "div[ng-if=\"subscribeSuccessCtrl.selectedPlan.type == 'ongoing'\"]",
                                          timeout=6)
    return element


def click_ok_button(web_driver):
    wait_for_element_visibility(web_driver, "css_selector", "button[ui-sref='main.home']").click()