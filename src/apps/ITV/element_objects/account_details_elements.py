from src.apps.utils import wait_for_element_visibility


def click_cancel_subscription(web_driver):
    wait_for_element_visibility(web_driver, "css_selector",
                                "a[ng-click='subscriptionCtrl.cancelSubscription(subscriptionCtrl."
                                "currentPlan.plan_id)']").click()