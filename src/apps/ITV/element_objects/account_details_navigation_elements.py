from src.apps.utils import wait_for_element_visibility


def personal_details(web_driver):
    wait_for_element_visibility(web_driver, "css_selector",
                                "a[ui-sref='main.logged.account.details']").click()


def change_email(web_driver):
    wait_for_element_visibility(web_driver, "css_selector",
                                "a[ui-sref='main.logged.account.change-email']").click()


def change_password(web_driver):
    wait_for_element_visibility(web_driver, "css_selector",
                                "a[ui-sref='main.logged.account.change_password']").click()


def payment_methods(web_driver):
    wait_for_element_visibility(web_driver, "css_selector",
                                "a[ui-sref='main.logged.account.payment_methods']").click()


def transaction_history(web_driver):
    wait_for_element_visibility(web_driver, "css_selector",
                                "a[ui-sref='main.logged.account.transaction-history']").click()


def manage_subscriptions(web_driver):
    wait_for_element_visibility(web_driver, "css_selector",
                                "a[ui-sref='main.logged.account.manage-subscription']").click()


def device_management(web_driver):
    wait_for_element_visibility(web_driver, "css_selector",
                                "a[ui-sref='main.logged.account.devices']").click()
