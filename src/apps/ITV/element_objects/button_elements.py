from src.apps.utils import wait_for_element_visibility


def click_submit_button(web_driver):
    wait_for_element_visibility(web_driver, "css_selector", "button[type^='submit']").click()