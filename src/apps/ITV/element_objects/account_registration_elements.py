from random import randrange

from src.apps.utils import wait_for_element_visibility
from selenium.webdriver.support.select import Select


def click_sign_up_link(web_driver):
    wait_for_element_visibility(web_driver, "css_selector", "a[ui-sref='main.signup']").click()


def enter_email(web_driver, email):
    element = wait_for_element_visibility(web_driver, "name", "email")
    element.send_keys(email)


def enter_password(web_driver, password):
    element = wait_for_element_visibility(web_driver, "name", "password")
    element.send_keys(password)


def select_random_country_of_residence(web_driver):
    country_list = Select(wait_for_element_visibility(web_driver, "name", "countryOfResidence"))
    all_countries = country_list.options
    country_index = randrange(1, len(all_countries))
    country_list.select_by_visible_text(all_countries[country_index].text)