from src.apps.utils import wait_for_element_visibility


def choose_holiday_pass_plan(web_driver, ):
    wait_for_element_visibility(web_driver, "css_selector", "a[ng-click=\"selectPlanCtrl.select('holiday')\"]").click()


def choose_ongoing_plan(web_driver):
    wait_for_element_visibility(web_driver, "css_selector", "a[ng-click=\"selectPlanCtrl.select('ongoing')\"]").click()


def click_continue_button(web_driver):
    wait_for_element_visibility(web_driver, "css_selector", "a[ng-click=\"selectPlanCtrl.continue()\"]").click()