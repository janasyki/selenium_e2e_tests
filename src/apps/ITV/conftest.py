import pytest
import src.apps.ITV.element_objects.home_page_elements as home_page
import src.apps.ITV.element_objects.account_registration_elements as account_registration
import src.apps.ITV.element_objects.button_elements as actions
import src.apps.ITV.element_objects.card_details_page_elements as card_details_page
import src.apps.ITV.element_objects.select_subscription_page_elements as select_plan_page


@pytest.fixture
def register_new_user(web_driver):
    """
    :param web_driver: Web Driver instance
    :return: User account has been created and user is logged in.
    """
    def register(email, password):
        home_page.click_sign_in_button(web_driver)
        account_registration.click_sign_up_link(web_driver)
        account_registration.enter_email(web_driver, email)
        account_registration.enter_password(web_driver, password)
        account_registration.select_random_country_of_residence(web_driver)
        actions.click_submit_button(web_driver)
    return register


@pytest.fixture
def go_to_account_details(web_driver):
    home_page.click_itv_logo(web_driver)
    home_page.click_sign_in_button(web_driver)
    home_page.click_account_details(web_driver)


@pytest.fixture
def submit_card_details(web_driver):
    """
    :param web_driver: Web Driver instance
    :return: User card details have been submitted. Finalize purchase has been triggered.
    """
    def submit(card_name, card_number, expiry_month, expiry_year, cvv):
        card_details_page.enter_card_name(web_driver, card_name)
        card_details_page.enter_card_number(web_driver, card_number)
        card_details_page.enter_expiry_date_month(web_driver, expiry_month)
        card_details_page.enter_expiry_date_year(web_driver, expiry_year)
        card_details_page.enter_cvv_code(web_driver, cvv)
        actions.click_submit_button(web_driver)
    return submit


@pytest.fixture
def choose_plan(web_driver):
    def chosen_plan(plan):
        """
        :param plan: 'holiday' for Holiday Pass and 'ongoing' for Monthly Ongoing
        """
        if plan == 'holiday':
            select_plan_page.choose_holiday_pass_plan(web_driver)
        if plan == 'ongoing':
            select_plan_page.choose_ongoing_plan(web_driver)
        select_plan_page.click_continue_button(web_driver)
    return chosen_plan