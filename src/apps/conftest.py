import pytest
from selenium import webdriver

browsers = {
    'firefox': webdriver.Firefox
}


@pytest.fixture(scope='module')
def url():
    return 'https://staging-itvessentials.sd-ngp.net/#dont-miss'


@pytest.fixture(scope='module', params=browsers.keys())
def driver(request):
    browser = browsers[request.param]()
    request.addfinalizer(lambda *args: browser.quit())
    return browser


@pytest.fixture
def web_driver(driver, url):
    browser = driver
    browser.set_window_size(1200, 800)
    browser.get(url)
    return browser

