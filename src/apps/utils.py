from selenium.webdriver.common.by import By
import selenium.webdriver.support.expected_conditions as EC
import selenium.webdriver.support.ui as ui


locator = {
    'xpath': By.XPATH,
    'css_selector': By.CSS_SELECTOR,
    'id': By.ID,
    'link': By.LINK_TEXT,
    'name': By.NAME,
    'tag': By.TAG_NAME,
    'partial': By.PARTIAL_LINK_TEXT
}


def wait_for_element_visibility(web_driver, locate_method, element, timeout=3):
    """
    :param web_driver: driver instance
    :param locate_method: locate_method
    :param element: element to wait for
    :param timeout: timeout in seconds. If timeout exceeded a Timeout Error will be raised

    This function will wait until until the element is present and return it. If timeout time exceeded a Timeout Error
    will be raised.
    """

    elem = ui.WebDriverWait(web_driver, timeout).until(
        EC.visibility_of_element_located((locator[locate_method], element)),
        message="can't find {}".format(element))
    return elem